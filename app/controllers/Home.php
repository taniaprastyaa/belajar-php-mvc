<?php
class Home extends Controller{
    public function index(){

        if(!isset($_SESSION["login"])){
            header("location:".BASE_URL."/auth/login");
            exit;
        }

        $data['judul'] = 'Home';
        $this->view('templates/header', $data);
        $this->view('templates/nav-sidebar', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);
    }

    public function about($company = 'SMKN1'){
        if(!isset($_SESSION["login"])){
            header("location:".BASE_URL."/auth/login");
            exit;
        }
        $data["judul"] = 'About';
        $data["company"] = $company;
        $this->view('templates/header', $data);
        $this->view('templates/nav-sidebar', $data);
        $this->view("home/index", $data);
        $this->view("templates/footer", $data);
    }

    public function profile(){
        if(!isset($_SESSION["login"])){
            header("location:".BASE_URL."/auth/login");
            exit;
        }
        $data['judul'] = 'Home';
        $this->view('templates/header', $data);
        $this->view('templates/nav-sidebar', $data);
        $this->view('home/profile', $data);
        $this->view('templates/footer', $data);
    }

}