<?php
class Blog extends Controller{
    public function index(){
        if(!isset($_SESSION["login"])){
            header("location:".BASE_URL."/auth/login");
            exit;
        } 

        $data['judul'] = 'Blog';
        $data['blogs'] = $this->model('blog_model')->getAllBlog();
        $this->view('templates/header', $data);
        $this->view('templates/nav-sidebar', $data);
        $this->view('blog/index', $data);
        $this->view('templates/footer', $data);

    }

    public function tambah(){
        if( $this->model('Blog_model')->buatArtikel($_POST) > 0){
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: ' . BASE_URL . '/blog');
            exit;
        } else {
            Flasher::setFlash('gagal', 'ditambahkan', 'danger');
            header('Location: ' . BASE_URL . '/blog');
            exit;
        }
    }

    public function hapus($id){
        if( $this->model('Blog_model')->hapusDataArtikel($id) > 0){
            Flasher::setFlash('berhasil', 'dihapus', 'success');
            header('Location: ' . BASE_URL . '/blog');
            exit;
        } else {
            Flasher::setFlash('gagal', 'dihapus', 'danger');
            header('Location: ' . BASE_URL . '/blog');
            exit;
        }

    }

    function redirect($string){
        header('Location: ' . BASE_URL . '/' . $string);
        exit;
    }

}