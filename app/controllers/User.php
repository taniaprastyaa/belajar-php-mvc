<?php
class User extends Controller {
    public function index(){
        if(!isset($_SESSION["login"])){
            header("location:".BASE_URL."/auth/login");
            exit;
        }
        $data['judul'] = 'Halaman Pengguna';
        $data['users'] = $this->model('User_model')->getAllUser();
        $this->view('templates/header', $data);
        $this->view('templates/nav-sidebar', $data);
        $this->view('user/index', $data);
        $this->view('templates/footer', $data);
    }

    // public function tambah(){
    //     // $this->model('User_model')->tambahDataUser($_POST);
    //     if( $this->model('User_model')->tambahDataUser($_POST) > 0){
    //         header('Location: ' . BASE_URL . '/user');
    //         exit;
    //     } else {
    //         header('Location: ' . BASE_URL . '/user');
    //         exit;
    //     }
    // }
}