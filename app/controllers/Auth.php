<?php
class Auth extends controller {
    public function index()
    {
        $data['judul'] = 'Register';
        $this->view('register_form', $data);
    }

    public function register_post()
    {

        if(isset($_POST['password']) && ($_POST['password'] == $_POST['password_verify'])){
            $user = $this->model('User_model')->addUser(
                $_POST['email'],
                $_POST['username'],
                $_POST['password'],
                $_POST['firstname'],
                $_POST['lastname']
            );
            if($user) {
                header('Location: ' . BASE_URL . '/auth/login');
            }
        }

	}

    public function login()
    {

        $data = [
            'judul' => 'Login',
            'username' => '',
            'password' => '',
            'errorMessage' =>''
        ];

        if (isset($_POST['username'])) {

            $data = [
                'username' => $_POST['username'],
                'password' => $_POST['password'],
                'errorMessage' => ''
            ];

            //Validate username
            if (empty($data['username'])) {
                $data['errorMessage'] = 'Please enter complete data!';
            }

            //Validate password
            if (empty($data['password'])) {
                $data['errorMessage'] = 'Please enter complete data!';
            }

            //Check errors are empty
            if (empty($data['errorMessage'])) {
                if($this->model('User_model')->findUserByEmailorUsername($data['username'], $data['username'])){
                    $loggedInUser = $this->model('User_model')->login($data['username'], $data['password']);

                    if ($loggedInUser) {
                        $this->createUserSession($loggedInUser);
                    } else {
                        $data['judul'] = 'Login';
                        $data['errorMessage'] = 'Password is incorrect. Please try again.';

                        $this->view('login_form', $data);
                    }
                } else {
                    $data['errorMessage'] = 'Username is incorrect. Please try again.';
                }
            }
        } else {
            $data = [
                'judul' => 'Login',
                'username' => '',
                'password' => '',
                'errorMessage' => ''
            ];
        }

        $data['judul'] = 'Login';
        $this->view('login_form', $data);
    }

    public function createUserSession($user)
    {
        $_SESSION['login'] = true;
        $_SESSION['user_id'] = $user['user_id'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['firstname'] = $user['firstname'];
        $_SESSION['lastname'] = $user['lastname'];


        header('location:' . BASE_URL . '/home/index');
    }

}