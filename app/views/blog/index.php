<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-4">
    <div class="row">
        <div class="col-lg-6">
            <?php FLasher::flash();?>
        </div>
    </div>

    <div class="col-lg-6 mt-4">
        <button type="button" class="btn btn-primary tombolTambahData" data-bs-toggle="modal" data-bs-target="#formModal" style="margin-bottom: 25px;">
                Buat Artikel
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Judul</th>
              <th scope="col">Slug</th>
              <th scope="col">Konten</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
                <?php foreach( $data['blogs'] as $blog) : ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $blog["title"] ?></td>
                    <td><?= $blog["slug"] ?></td>
                    <td><?= $blog["content"] ?></td>
                    <td>                    <li class="list-group-item">
                        <a href="<?= BASE_URL; ?>/blog/hapus/<?= $blog['id'];?>" class="badge text-bg-danger" style="text-decoration: none;">hapus</a> 
                    </td>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>

    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModalLabel">Tambah Data Artikel</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="<?= BASE_URL; ?>/blog/tambah" method="post">
                        <input type="hidden" name="id" id="id">
                        <div class="mb-3">
                            <label for="nama" class="form-label">Judul</label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>

                        <div class="mb-3">
                            <label for="nrp" class="form-label">Slug</label>
                            <input type="text" class="form-control" id="slug" name="slug">
                        </div>

                        <div class="mb-3">
                            <label for="email" class="form-label">Konten</label>
                            <textarea class="form-control" id="content" rows="3" name="content"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>