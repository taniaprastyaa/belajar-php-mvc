<?php
class Blog_model {
    private $table = 'blogs';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllBlog(){
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }

    public function buatArtikel($data){
        $query = "INSERT INTO {$this->table}
                    VALUES
                  (null, :title, :slug, :content)";

        $this->db->query($query);
        $this->db->bind('title', $data['title']);
        $this->db->bind('slug', $data['slug']);
        $this->db->bind('content', $data['content']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function hapusDataArtikel($id){
        
        $query = "DELETE FROM {$this->table} WHERE id = :id";

        $this->db->query($query);
        $this->db->bind('id', $id);

        $this->db->execute();
        return $this->db->rowCount();
    }
}