<?php 
class User_model{
    private $table = 'users';
    private $db;

    private $name = 'Tania Prastya';
    
    public function __construct(){
        $this->db = new Database;
    }

    public function getUser(){
        return $this->name;
    }


    public function getAllUser(){
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }

    public function getUserById($user_id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE user_id=:user_id");
        $this->db->bind('user_id', $user_id);
        return $this->db->resultSingle();
    }

    public function findUserByEmailorUsername($username, $email){
        $this->db->query("SELECT * FROM {$this->table} WHERE `username` =:username or `email` =:email");
        $this->db->bind('username', $username);
        $this->db->bind('email', $email);
        $row = $this->db->resultSingle();

        if($this->db->rowCount() > 0){
            return $row;
        } else {
            return false;
        }
    }

    public function login($username, $password){
        $row = $this->findUserByEmailorUsername($username, $username);
        if( $row == false) return false;

        $hashedPassword = $row['password'];

        if (password_verify($password, $hashedPassword)) {
            return $row;
        } else {
            return false;
        }
    }

    public function addUser($email, $username, $password, $firstname = '', $lastname = ''){
        $email      = htmlspecialchars($email);
        $username   = htmlspecialchars($username);
        $password   = password_hash($password, PASSWORD_DEFAULT);
        $firstname  = htmlspecialchars($firstname);
        $lastname   = htmlspecialchars($lastname);
        $query      = "INSERT INTO users 
                        VALUES
                    (null, :email, :username, :password, :firstname, :lastname)";

        $this->db->query($query);
        $this->db->bind('email', $email);
        $this->db->bind('username', $username);
        $this->db->bind('password', $password);
        $this->db->bind('firstname', $firstname);
        $this->db->bind('lastname', $lastname);

        $this->db->execute();
        return $this->db->rowCount();

    }

}




